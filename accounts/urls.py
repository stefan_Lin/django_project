from django.urls import path

from . import views

urlpatterns = [
    # path("", views.index, name="index"),
    path('login/', views.login.as_view(), name='user-login'),
    # path('user/permission/', views.UserPermissionList.as_view(), name='user-permission'),
    # path('user/change-password/', views.ChangePassword.as_view(), name='change-password'),
    # path('user/forget-password/', views.ForgetPassword.as_view(), name='change-password'),
    path('account_users/', views.GetUsers.as_view(), name='get-users-list'),
]
