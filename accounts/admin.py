from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.utils.translation import gettext, gettext_lazy as _, ugettext
from .models import User, Group


# Register your models here.

class UserAdmin(UserAdmin):
    # form = AdminUserChangeForm
    # add_form = AdminUserCreationForm

    list_display = ('username', 'full_name', 'email', 'is_staff', 'activated', 'group_list')
    list_filter = ['activated', 'is_staff', 'groups']
    search_fields = ('username', 'full_name', 'email')

    def group_list(self, obj):
        groups = obj.groups.all()
        group_list = [i.name for i in groups]
        return ', '.join(group_list)

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('full_name', 'first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'activated'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2'),
        }),
    )

class NewGroupAdmin(GroupAdmin):
    list_display = ['id', 'name']

admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
admin.site.register(Group, NewGroupAdmin)
# admin.site.register(Question)
# admin.site.register(Choice)
