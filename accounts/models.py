import uuid
from django.utils.translation import gettext_lazy as _
from django.db import models

# Create your models here.

from django.contrib.auth.models import AbstractUser, Group, Permission, User

# def __group_str(self):
#     return '%s - %s' % (self.pk, self.name)

# Group.add_to_class("__str__", __group_str)

class UserGroup(models.Model):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    group = models.ForeignKey('auth.Group', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'accounts_user_groups'

class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    full_name = models.CharField(blank=True, null=True, max_length=256)
    email = models.EmailField(blank=True, null=True, unique=True)
    activated = models.BooleanField(default=False)
    groups = models.ManyToManyField(
        'auth.Group',
        verbose_name=_('groups'),
        blank=True,
        help_text=_(
            'The groups this user belongs to. A user will get all permissions '
            'granted to each of their groups.'
        ),
        related_name="user_set",
        related_query_name="user",
        through="UserGroup"
    )

    class Meta:
        db_table = 'accounts_user'
