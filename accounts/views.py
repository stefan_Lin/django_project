from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from django.http import HttpResponse
from django.contrib.auth import get_user_model, authenticate
from django.shortcuts import get_object_or_404

from .serializers import UserSerializer

from .models import User
# Create your views here.


# def index(request):
#     return HttpResponse("Hello, world. You're at the polls index.")

class login(APIView):

    def post(self, request, *args, **kwargs):
        print(">>>"*8, request.data)
        return self.get_auth_user(request.data)

    def get_auth_user(self, account):
        user = get_object_or_404(User, email=account['email'], activated=True)
        auth_user = authenticate(username=user.username, password=account['password'])
        if not user:
            raise Exception
        return Response(
            { 'msg': 'SUCCESS' },
            status=status.HTTP_200_OK
        )

class GetUsers(ListAPIView):
    '''
    Return json:
        {[
            {id, username, full_name},
            {id, username, full_name},
            ...
        ]} 
    '''
    # permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get_queryset(self):
        queryset = get_user_model().objects.all()

        return queryset
