from rest_framework import serializers
from django.contrib.auth import get_user_model


class PasswordSerializer(serializers.Serializer):
    old = serializers.CharField(max_length=128)
    new = serializers.CharField(max_length=128)


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = (
            'id',
            'username',
            'full_name',
        )
