from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
from django.db import transaction, IntegrityError

from rest_framework.exceptions import ValidationError
from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.response import Response
from rest_framework import status
from .serializer import (
    HomepageNewsSerializer,
    HomepageAboutSerializer,
    HomepageFAQSerializer,
    )
from .models import Question, News

# Create your views here.

def index(request):
    latest_question_list = Question.objects.order_by("-pub_date")[:5]
    template = loader.get_template("index.html")
    context = {
        "latest_question_list": latest_question_list,
    }
    return HttpResponse(template.render(context, request))

def detail(request, question_id):
    return HttpResponse("You're looking at question %s." % question_id)


def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)


def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)

class NewsList(ListAPIView):
    # pagination_class = CustomPageNumberPagination
    serializer_class = HomepageNewsSerializer

    def get_queryset(self):
        news = News.objects.all()
        return news
    
    def post(self, request, *args, **kwargs):
        news_detail = request.data
        serializer = HomepageNewsSerializer(data=news_detail)
        if not serializer.is_valid():
            raise ValidationError(serializer.errors)
        news_instance = serializer.save()

        return Response({
            "status_code": status.HTTP_200_OK,
            "result": {},
        })
    
class NewsDetail(RetrieveUpdateAPIView):
    serializer_class = HomepageNewsSerializer
    queryset = News.objects.all()

    # def get_queryset(self):
    #     return diversion_role(self.request.user)

    def get(self, request, pk, *args, **kwargs):
        print(">>>>>"*8)
        response_data = self.retrieve(request, pk).data
        
        return Response(response_data)

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        request_payload = request.data

        with transaction.atomic():
            serializer = HomepageNewsSerializer(instance=instance, data=request_payload)
        if not serializer.is_valid():
            raise ValidationError(serializer.errors)
        serializer.save(
            content="測試"
        )

        return Response({
            "status_code": status.HTTP_200_OK,
            "result": {}
        })

class GetAboutView(ListAPIView):
    # pagination_class = CustomPageNumberPagination
    queryset = News.objects.all()
    serializer_class = HomepageAboutSerializer

    def get_queryset(self):
        news = News.objects.all()
        return news
    
    def post(self, request, *args, **kwargs):
        print(">>>"*8, self.queryset)
        news_detail = request.data
        serializer = HomepageAboutSerializer(data=news_detail)
        if not serializer.is_valid():
            raise ValidationError(serializer.errors)
        news_instance = serializer.save()

        return Response({
            "status_code": status.HTTP_200_OK,
            "result": {},
        })
    
class FAQList(ListAPIView):
    # pagination_class = CustomPageNumberPagination
    serializer_class = HomepageFAQSerializer

    def get_queryset(self):
        news = News.objects.all()
        return news
    
    def post(self, request, *args, **kwargs):
        news_detail = request.data
        serializer = HomepageFAQSerializer(data=news_detail)
        if not serializer.is_valid():
            raise ValidationError(serializer.errors)
        news_instance = serializer.save()

        return Response({
            "status_code": status.HTTP_200_OK,
            "result": {},
        })
