from rest_framework import serializers
from .models import News, About, FAQ

class HomepageNewsSerializer(serializers.ModelSerializer):

    class Meta:
        model = News
        fields = '__all__'

class HomepageAboutSerializer(serializers.ModelSerializer):

    class Meta:
        model = About
        fields = '__all__'

class HomepageFAQSerializer(serializers.ModelSerializer):

    class Meta:
        model = FAQ
        fields = '__all__'
