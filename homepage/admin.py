from django.contrib import admin
from .models import Question, About, FAQ, News

# Register your models here.

class AbiutUsAdmin(admin.ModelAdmin):
    list_display = ['id', 'content1', 'status', 'dispaly', 'updated_user']

class FAQAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'content', 'status', 'display']

class NewsAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'content', 'type', 'is_index', 'launch_start_time', 'launch_end_time']


admin.site.register(About, AbiutUsAdmin)
admin.site.register(FAQ, FAQAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(Question)
