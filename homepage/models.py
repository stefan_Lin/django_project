from django.db import models
from django.utils import timezone
from applications import conf_choices


class News(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    type = models.CharField(max_length=20,
        choices=conf_choices.NewsTypeChoices.choices,
    )
    # status = models.CharField(
    #     max_length=20,
    #     choices=conf_choices.NewsStatusChoices.choices,
    # )
    is_index = models.BooleanField(default=True)
    launch_start_time = models.DateTimeField(null=True, blank=True)
    launch_end_time = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'homepage_news'

class FAQ(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    status = models.CharField(max_length=20)
    display = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)
    # delete_at = models.DateTimeField(null=True, blank=True)
    # category = models.ForeignKey(DocCategory, on_delete=models.SET_NULL, blank=True, null=True, related_name='category_faq')

    def __str__(self):
        return f"""{self.pk}"""

class About(models.Model):
    content1 = models.TextField()
    content2 = models.TextField()
    content3 = models.TextField()
    image = models.CharField(max_length=256, null=True)
    image2 = models.CharField(max_length=256, null=True)
    image3 = models.CharField(max_length=256, null=True)
    status = models.CharField(max_length=20)
    dispaly = models.BooleanField(default=False)
    updated_user = models.IntegerField()
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"""{self.pk}"""

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField("date published")

    def __str__(self):
        return self.question_text
