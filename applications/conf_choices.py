from django.utils.translation import gettext_lazy as _
from django.db import models

# class NewsStatusChoices(models.TextChoices):
#     PENDINGPAYMENT = 'pending_payment', _('待付款')
#     PROCESSING = 'processing', _('處理中')

class NewsTypeChoices(models.TextChoices):
    NEWS = 'news', _('最新消息')
    ANNOUNCEMENT = 'announcement', _('公告')
